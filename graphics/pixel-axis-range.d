float[] floatRange(int len) {
    float[] result= [];
    foreach(i; 0 .. len)
        result ~= [(i * 2f / (len -1)) -1];
    return result;
};

void main(string[] av) {
    import std.stdio;
    writeln(floatRange(48));
};
