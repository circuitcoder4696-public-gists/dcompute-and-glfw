
void triangle(Frame frame, vec4f a, vec4f b, vec4f c, vec4f color) {
    import std.stdio;
    foreach(yi,y; floatRange(frame.height, true))foreach(xi,x; floatRange(frame.width)) {
        if(xi == 0)writeln();
        if(checkPixel(a, b, c, x, y, 0)) write("*");
        else write("█");
    };
};

void main(string[] av) {
    triangle(frame,
             vec4f(-1  ,-0.5,0f,0f),
             vec4f(-1  , 2  ,0f,0f),
             vec4f( 1  , 1  ,0f,0f),
    vec4f(-0.5,-0.5,0f,0f));
};

// ------------------------------------------------

struct Frame {
    int width,height;
    public this(int width, int height) {
        this.width= width;
        this.height= height;
    };
};

Frame frame= Frame(75,36);

struct vec2i {
    int x,y;
};

struct vec4f {
    float x,y,z;
    float t;
};

float[] floatRange(int len, bool invert= false) {
    float[] result= [];
    foreach(i; 0 .. len)
        result ~= [(invert ? 1 : -1) - (i * (invert ? 2f : -2f) / (len -1))];
    return result;
};

bool checkPixel(vec4f a, vec4f b, vec4f c, float x, float y, float z) {
    //   `y<\frac{B[1]-A[1]}{B[0]-A[0]}x+B[1]-\frac{B[1]-A[1]}{B[0]-A[0]}\cdot B[0]`.  
    if(a[0] == b[0]) {
        if(a[0] > x)return false;
    }else{
        if(a[0] < b[0]){
            if(((x * (b[1] - a[1]) / (b[0] - a[0])) - (b[0] * (b[1] - a[1]) / (b[0] - a[0]))) < y)return false;
        }else{
            if(((x * (b[1] - a[1]) / (b[0] - a[0])) - (b[0] * (b[1] - a[1]) / (b[0] - a[0]))) > y)return false;
        };
    };
    if(a[0] == c[0]) {
        if(a[0] > x)return false;
    }else{
        if(a[0] > c[0]){
            if(((x * (c[1] - a[1]) / (c[0] - a[0])) - (c[0] * (c[1] - a[1]) / (c[0] - a[0]))) < y)return false;
        }else{
            if(((x * (c[1] - a[1]) / (c[0] - a[0])) - (c[0] * (c[1] - a[1]) / (c[0] - a[0]))) > y)return false;
        };
    };
    if(b[0] == c[0]) {
        if(b[0] > x)return false;
    }else{
        if(b[0] < c[0]){
            if(((x * (c[1] - b[1]) / (c[0] - b[0])) - (c[0] * (c[1] - b[1]) / (c[0] - b[0]))) < y)return false;
        }else{
            if(((x * (c[1] - b[1]) / (c[0] - b[0])) - (c[0] * (c[1] - b[1]) / (c[0] - b[0]))) > y)return false;
        };
    };
    return true;
};
