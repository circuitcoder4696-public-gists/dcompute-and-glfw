import std.stdio:writeln;
import std.traits:Fields;

ubyte[] dcgMem= new ubyte[2_400_000];

struct ctDecoder {
    static string genCode(S)() {
        S s= S();
        string code= "";
        code ~= "    public void data(void[] input)@property {\n        ulong ptr= cast(ulong) input.ptr;";
        foreach(i,field;S().tupleof) {
            code ~= "\n        this."~s.tupleof[i].stringof[(s.stringof.length +1)..$]~"= *(cast("~typeof(s.tupleof[i]).stringof~"*) ptr);";
            code ~= "\n        ptr += "~s.tupleof[i].sizeof.stringof~";";
        };
        code ~= "\n    };";
        return code;
    };
};

struct ctEncoder {
    static string genCode(S)() {
        S s= S();
        string code= "";
        code ~= "    public void[] data()@property {\n        void[] output= [];";
        foreach(i,field;S().tupleof)
            code ~= "\n        output ~= cast(void[]) (&this."~s.tupleof[i].stringof[(s.stringof.length +1)..$]~")[0 .. 1];";
        code ~= "\n        return output;\n    };";
        return code;
    };
};

struct ctSize {
    static string genCode(S)() {
        S s= S();
        string code= "";
        code ~= "    public size_t size()@property {\n        size_t output;";
        foreach(i,field;S().tupleof)
            code ~= "\n        output += "~typeof(s.tupleof[i]).stringof~".sizeof;";
        code ~= "\n        return output;\n    };";
        return code;
    };
};

struct Vert {
    float x,y,z;
    float u,v;
    mixin(ctDecoder.genCode!Vert);
    mixin(ctEncoder.genCode!Vert);
    pragma(msg,ctSize.genCode!Vert);
};

struct GraphicsImpl {
    public static void vertShaderImpl_drawTriangle(int width, int height, ref ubyte[] data, Vert a, Vert b, Vert c) {
        ulong i;
        foreach(x; 0 .. width)foreach(y; 0 .. height) {
            if(y < ((a.y - b.y) * x + (a.x - b.x)))continue;
            i = x + y * width;
            data[i * 4 +0]= 255;
            data[i * 4 +1]= 85;
            data[i * 4 +2]= 0;
            data[i * 4 +3]= 255;
        };
        float slope1= (a.y - b.y) / (a.x - b.x);
    };
};

void main(string[] av) {
    void[] coord= [];
    coord ~= Vert(-0.5,-0.5,0, 0,0).data;
    coord ~= Vert(-0.5, 0.5,0, 0,0).data;
    coord ~= Vert( 0.5, 0.5,0, 0,0).data;
    coord ~= Vert( 0.5,-0.5,0, 0,0).data;
    writeln(coord);
};
