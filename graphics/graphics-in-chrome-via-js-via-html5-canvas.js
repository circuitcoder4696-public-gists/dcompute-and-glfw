floatRange= ((len, invert)=>{
    result= new Float32Array(len);
    var i;
    for(i=0;i<len;i++)result[i]= [-1,1][1* invert] - (i * [-2,2][1* invert] / (len -1));
    return result;
});
checkPixel= ((a, b, c, x, y, z)=>{
    //   `y<\frac{B[1]-A[1]}{B[0]-A[0]}x+B[1]-\frac{B[1]-A[1]}{B[0]-A[0]}\cdot B[0]`.  
    if(a[0] == b[0]) {
        if(a[0] > x)return false;
    }else{
        if(a[0] < b[0]){
            if(((x * (b[1] - a[1]) / (b[0] - a[0])) - (b[0] * (b[1] - a[1]) / (b[0] - a[0]))) < y)return false;
        }else{
            if(((x * (b[1] - a[1]) / (b[0] - a[0])) - (b[0] * (b[1] - a[1]) / (b[0] - a[0]))) > y)return false;
        };
    };
    if(a[0] == c[0]) {
        if(a[0] > x)return false;
    }else{
        if(a[0] > c[0]){
            if(((x * (c[1] - a[1]) / (c[0] - a[0])) - (c[0] * (c[1] - a[1]) / (c[0] - a[0]))) < y)return false;
        }else{
            if(((x * (c[1] - a[1]) / (c[0] - a[0])) - (c[0] * (c[1] - a[1]) / (c[0] - a[0]))) > y)return false;
        };
    };
    if(b[0] == c[0]) {
        if(b[0] > x)return false;
    }else{
        if(b[0] < c[0]){
            if(((x * (c[1] - b[1]) / (c[0] - b[0])) - (c[0] * (c[1] - b[1]) / (c[0] - b[0]))) < y)return false;
        }else{
            if(((x * (c[1] - b[1]) / (c[0] - b[0])) - (c[0] * (c[1] - b[1]) / (c[0] - b[0]))) > y)return false;
        };
    };
    return true;
});
rasterize= ((width, height, a, b, c)=>{
    frameBuffer= new Int8Array(width * height * 4);
    var triangle,background;
    triangle= [255,85,0,255];
    background= [0,170,255,255];
    var i,xi,yi,x,y,X,Y;
    X= floatRange(width, false);
    Y= floatRange(height, false);
    Z= [0];
    for(xi=0;xi<width;xi++)for(yi=0;yi<height;yi++) {
        i= xi + yi * width;
        x= X[xi];
        y= Y[yi];
        if(checkPixel(
            [-0.5,-0.5,0,0],
            [-0.5, 0.5,0,0],
            [ 0.5, 0.5,0,0],
        x,y,0)) {
            frameBuffer[i * 4 + 0]= triangle[0];
            frameBuffer[i * 4 + 1]= triangle[1];
            frameBuffer[i * 4 + 2]= triangle[2];
            frameBuffer[i * 4 + 3]= triangle[3];
        } else {
            frameBuffer[i * 4 + 0]= background[0];
            frameBuffer[i * 4 + 1]= background[1];
            frameBuffer[i * 4 + 2]= background[2];
            frameBuffer[i * 4 + 3]= background[3];
        };
    };
})(25, 18);
((w,h, code, data)=>{
    document.body.appendChild(((elem)=>{
        elem.setAttribute("width", w);
        elem.setAttribute("height", h);
        elem.setAttribute('id', "canvas_2d_api");
        canvasCTX= elem.getContext('2d');
        canvasCTX.canvas.mem= canvasCTX.createImageData(w,h);
        canvasCTX.canvas.data= canvasCTX.canvas.mem.data;
        code(canvasCTX, data);
        return elem;
    })(document.createElement("CANVAS")));
})(1200,500, (ctx, data)=>{
    rasterize(ctx.width,ctx.height,
        [-0.5,-0.5,0,0],
        [-0.5, 0.5,0,0],
        [ 0.5, 0.5,0,0]);
    ctx.putImageData(ctx.canvas.mem, 0,0);
}, frameBuffer);
