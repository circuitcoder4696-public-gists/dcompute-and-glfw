struct Frame {
    int width,height;
    ubyte[] data;
    ubyte[4] backgroundColor;
    ulong triangleCount;
    public this(int width, int height) {
        this.width= width;
        this.height= height;
        this.data= new ubyte[width * height * 4];
    };
    public void setBackgroundColor(float red, float green, float blue, float alpha) {
        this.backgroundColor[0]= 0;
        if(0f < red || 1f >= red)this.backgroundColor[0]= cast(ubyte) (255 * red);
        if(red>1f)this.backgroundColor[0]= 255;
        this.backgroundColor[1]= 0;
        if(0f < green || 1f >= green)this.backgroundColor[1]= cast(ubyte) (255 * green);
        if(green>1f)this.backgroundColor[1]= 255;
        this.backgroundColor[2]= 0;
        if(0f < blue || 1f >= blue)this.backgroundColor[2]= cast(ubyte) (255 * blue);
        if(blue>1f)this.backgroundColor[2]= 255;
        this.backgroundColor[3]= 0;
        if(0f < alpha || 1f >= alpha)this.backgroundColor[3]= cast(ubyte) (255 * alpha);
        if(alpha>1f)this.backgroundColor[3]= 255;
    };
};

Frame frame= Frame(25,15);

struct ctDecoder {
    static string genCode(S)(S s) {
        string code= "";
        code ~= "    public void data(void[] input)@property {\n        ulong ptr= cast(ulong) input.ptr;";
        foreach(i,field;S().tupleof) {
            code ~= "\n        this."~s.tupleof[i].stringof[(s.stringof.length +1)..$]~"= *(cast("~typeof(s.tupleof[i]).stringof~"*) ptr);";
            code ~= "\n        ptr += "~s.tupleof[i].sizeof.stringof~";";
        };
        code ~= "\n    };";
        return code;
    };
};

struct ctEncoder {
    static string genCode(S)(S s) {
        string code= "";
        code ~= "    public void[] data()@property {\n        void[] output= [];";
        foreach(i,field;S().tupleof)
            code ~= "\n        output ~= cast(void[]) (&this."~s.tupleof[i].stringof[(s.stringof.length +1)..$]~")[0 .. 1];";
        code ~= "\n        return output;\n    };";
        return code;
    };
};

struct ctSize {
    static string genCode(S)(S s) {
        string code= "";
        code ~= "    public size_t size()@property {\n        size_t output;";
        foreach(i,field;S().tupleof)
            code ~= "\n        output += "~typeof(s.tupleof[i]).stringof~".sizeof;";
        code ~= "\n        return output;\n    };";
        return code;
    };
};

struct ctVector {
    static string genCode(S)() {
        S s= S();
        string code= "//   Codex for various structures used in the graphics-API.  ";
        code ~= "\n"~ctDecoder.genCode(s);
        code ~= "\n"~ctEncoder.genCode(s);
        code ~= "\n"~ctSize.genCode(s);
        return code;
    };
};

struct vec2i {
    int x,y;
    mixin(ctVector.genCode!vec2i);
};

struct vec4f {
    float x,y,z;
    float t;
    mixin(ctVector.genCode!vec4f);
};

bool checkPixel(vec4f a, vec4f b, vec4f c, float x, float y, float z) {
    //   `y<\frac{B.y-A.y}{B.x-A.x}x+B.y-\frac{B.y-A.y}{B.x-A.x}\cdot B.x`.  
    if(a.x == b.x) {
        if(a.x > x)return false;
    }else{
        if(a.x < b.x){
            if(((x * (b.y - a.y) / (b.x - a.x)) - (b.x * (b.y - a.y) / (b.x - a.x))) < y)return false;
        }else{
            if(((x * (b.y - a.y) / (b.x - a.x)) - (b.x * (b.y - a.y) / (b.x - a.x))) > y)return false;
        };
    };
    if(a.x == c.x) {
        if(a.x > x)return false;
    }else{
        if(a.x > c.x){
            if(((x * (c.y - a.y) / (c.x - a.x)) - (c.x * (c.y - a.y) / (c.x - a.x))) < y)return false;
        }else{
            if(((x * (c.y - a.y) / (c.x - a.x)) - (c.x * (c.y - a.y) / (c.x - a.x))) > y)return false;
        };
    };
    if(b.x == c.x) {
        if(b.x > x)return false;
    }else{
        if(b.x < c.x){
            if(((x * (c.y - b.y) / (c.x - b.x)) - (c.x * (c.y - b.y) / (c.x - b.x))) < y)return false;
        }else{
            if(((x * (c.y - b.y) / (c.x - b.x)) - (c.x * (c.y - b.y) / (c.x - b.x))) > y)return false;
        };
    };
    return true;
};

void triangle(Frame frame, vec4f a, vec4f b, vec4f c, vec4f color) {
    import std.stdio;
    frame.triangleCount++;
    uint i;
    ubyte triangleState;
    foreach(y; 0 .. frame.height)foreach(x; 0 .. frame.width) {
        i= (y * frame.width + x) *4;
        if(checkPixel(a, b, c, x * 2f - frame.width, y * 2f - frame.height, 0, triangleState)) {
            write("*");
            frame.data[i+0]= cast(ubyte) (255 * color.x);
            frame.data[i+1]= cast(ubyte) (255 * color.y);
            frame.data[i+2]= cast(ubyte) (255 * color.z);
            frame.data[i+3]= 255;
        } else {
            if(frame.triangleCount == 0) {
                write("*");
                frame.data[i+0]= frame.backgroundColor[0];
                frame.data[i+1]= frame.backgroundColor[1];
                frame.data[i+2]= frame.backgroundColor[2];
                frame.data[i+3]= frame.backgroundColor[3];
            };
        };
    };
};

void main(string[] av) {
    import std.stdio;
    frame.triangleCount= 0;
    triangle(frame,
             vec4f(-0.5,-0.5,0f,0f),
             vec4f(-0.5, 0.5,0f,0f),
             vec4f( 0.5, 0.5,0f,0f),
    vec4f(-0.5,-0.5,0f,0f));
    frame.setBackgroundColor(0.2f,0.3f,0.5f,1f);
    writeln(frame);
};
