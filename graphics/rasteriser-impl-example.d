import std.stdio;
import std.algorithm;
import std.math;

struct Vec2i {
    int x, y;
    Vec2i opBinary(string op : "-")(Vec2i other) {
        return Vec2i(this.x - other.x, this.y - other.y);
    };
};

struct Vec3i {
    int x, y, z;
    public ubyte[] data_00000000()@property {
        ubyte[] result= new ubyte[4];
        result[0]= cast(ubyte) this.x;
        result[1]= cast(ubyte) this.y;
        result[2]= cast(ubyte) this.z;
        result[3]= 255;
        return result;
    };
};

struct Vec3f {
    float x, y, z;
};

int dot(Vec2i a, Vec2i b) { return a.x * b.x + a.y * b.y; };

bool barycentricInside(Vec2i p, Vec2i a, Vec2i b, Vec2i c) {
    Vec2i v0 = b - a, v1 = c - a, v2 = p - a;
    float px= cast(float) p.x;
    float py= cast(float) p.y;
    float ax= cast(float) a.x;
    float ay= cast(float) a.y;
    float bx= cast(float) b.x;
    float by= cast(float) b.y;
    float cx= cast(float) c.x;
    float cy= cast(float) c.y;
    float dot00 = cast(float)dot(v0, v0);
    float dot01 = cast(float)dot(v0, v1);
    float dot02 = cast(float)dot(v0, v2);
    float dot11 = cast(float)dot(v1, v1);
    float dot12 = cast(float)dot(v1, v2);
    float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
    float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
    float v = (dot00 * dot12 - dot01 * dot02) * invDenom;
    return (u >= 0) && (v >= 0) && (u + v < 1);
};

void rasterizeTriangle(ubyte[] pixels, int width, int height, Vec3f* clr, Vec2i t0, Vec2i t1, Vec2i t2) {
    foreach(x; 0 .. width) {
        foreach(y; 0 .. height) {
            if (barycentricInside(Vec2i(x, y), t0, t1, t2)) {
                pixels ~= Vec3i(cast(int)(clr.x * 255), cast(int)(clr.y * 255), cast(int)(clr.z * 255)).data_00000000;
            } else {
                pixels ~= Vec3i(cast(int)(clr.x * 0), cast(int)(clr.y * 0), cast(int)(clr.z * 0)).data_00000000;
            };
        };
    };
    writeln(pixels);
};

void main() {
    enum width = 25;
    enum height = 18;
    ubyte[] pixels= [];
    Vec2i t0 = Vec2i(1, 7), t1 = Vec2i(5, 16), t2 = Vec2i(7, 8);
    Vec3f clr = Vec3f(1.0f, 0.0f, 0.0f);
    rasterizeTriangle(pixels, width, height, &clr, t0, t1, t2);
    // Display or save the rasterized triangle
};
