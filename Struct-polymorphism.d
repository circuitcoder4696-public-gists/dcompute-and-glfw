
struct A {
    Obj super_= Obj();
    alias super_ this;
    int a;
    public int getSlopeValOfA() { return this.a * 2 - 3; };
};

struct B {
    A super_= A();
    alias super_ this;
    int b;
    int c;
    char[] d;
    float[] e;
};

struct C {
    B super_= B();
    alias super_ this;
    pragma(msg,Serializable!(bit32,HEAD).genCode(C()));
    float[] f;
    float g;
    float h;
};

void main() {
    import std.stdio;
    enum string x= "super_";
    C c= C();   //   Using this structure to help me fucking develop a fucking algorithm that can fucking generate fucking code to fucking decode a fucking array of fucking bytes.  
    c.a= 2;
    writeln(c.getSlopeValOfA());
    writeln(c.a);
    writeln(Serializable!(bit32,HEAD).genCode(c));
};

// ------------------------------------------------
// module core.Serializer.SerialDecoderImpl;

enum Bitness {none,Bit16,Bit32,Bit64};
public alias bit16= Bitness.Bit16;
public alias bit32= Bitness.Bit32;
public alias bit64= Bitness.Bit64;

private static bool hasSuper(S)() {
    bool result= false;
    S s= S();
    static foreach(i,v;s.tupleof)result |= (v.stringof=="super_");
    return result;
};

struct Obj {};

public enum StorePrimitivesIn : int {none,HEAD,BODY};
public alias HEAD= StorePrimitivesIn.HEAD;
public alias BODY= StorePrimitivesIn.BODY;

static struct SerialBuffWriterImpl(Bitness bitness, StorePrimitivesIn spi) {
    import std.array;
    import std.meta;
    import std.traits;
    private static string genArrayStaticCode(string host, string name, string type)() {
        return "\nthis."~name~"= ArrayUtil.gen!"~type[0 .. $ -2]~"(buff.readVal!"~["ushort","uint","ulong"][bitness-1]~" / "~type[0 .. $ -2]~".sizeof);";
    };
    private static string genArrayCode(string host, string name, string type)() {
        return "\nbuff.readArr("~name~");";
    };
    private static string genPrimitiveCode(string host, string name, string type)() {
        return "\nthis."~name~"= buff.readVal!"~type~";";
    };
    private static string _genCode(string host= "this", S)(S s) {
        string code= "";
        static foreach(index,field; s.tupleof)if(field.stringof != "super_")if(isDynamicArray!(Fields!S[index])) {
            code ~= genArrayStaticCode!(host, field.stringof, typeof(field).stringof);
        } else {
            static if(spi == HEAD)
                code ~= genPrimitiveCode!(host, field.stringof, typeof(field).stringof);
        };
        static foreach(index,field; s.tupleof)if(field.stringof != "super_")if(isDynamicArray!(Fields!S[index])) {
            code ~= genArrayCode!(host, field.stringof, typeof(field).stringof);
        } else {
            static if(spi == BODY)
                code ~= genPrimitiveCode!(host, field.stringof, typeof(field).stringof);
        };
        return code;
    };
    private static string genCode(string host= "result", S)(S s) {
        string code= "";
        static if(hasSuper!S) code ~= genCode!(host~".super_")(s.super_);
        code ~= _genCode!(host)(s);
        return code;
    };
};

static struct SerialBuffReaderImpl(Bitness bitness, StorePrimitivesIn spi) {
    import std.array;
    import std.meta;
    import std.traits;
    private static string genArrayStaticCode(string host, string name, string type)() {
        return "\nbuff.writeVal!"~["ushort","uint","ulong"][bitness-1]~"("~host~"."~name~" * "~type[0 .. $ -2]~".sizeof);";
    };
    private static string genArrayCode(string host, string name, string type)() {
        return "\nbuff.writeArr("~name~");";
    };
    private static string genPrimitiveCode(string host, string name, string type)() {
        return "\n"~host~"."~name~"= buff.writeVal!"~type~";";
    };
    private static string _genCode(string host, S)(S s) {
        string code= "";
        static foreach(index,field; s.tupleof)if(field.stringof != "super_")if(isDynamicArray!(Fields!S[index])) {
            code ~= genArrayStaticCode!(host, field.stringof, typeof(field).stringof);
        } else {
            static if(spi == HEAD)
                code ~= genPrimitiveCode!(host, field.stringof, typeof(field).stringof);
        };
        static foreach(index,field; s.tupleof)if(field.stringof != "super_")if(isDynamicArray!(Fields!S[index])) {
            code ~= genArrayCode!(host, field.stringof, typeof(field).stringof);
        } else {
            static if(spi == BODY)
                code ~= genPrimitiveCode!(host, field.stringof, typeof(field).stringof);
        };
        return code;
    };
    private static string genCode(string host= "result", S)(S s) {
        string code= "";
        static if(hasSuper!S) code ~= genCode!("result.super_")(s.super_);
        code ~= _genCode!(host)(s);
        return code;
    };
};

static struct SerialDecoderMethodImpl(Bitness bitness, StorePrimitivesIn spi) {
    import std.array;
    import std.meta;
    import std.traits;
    private static string _genCode(string host= "result", S)(S s) {
        string code= "";
        code ~= "\n"~S.stringof~" result= "~S.stringof~"();";
        code ~= SerialBuffReaderImpl!(bit32,HEAD).genCode(s);
        code ~= "\nreturn result;";
        return code;
    };
    private static string genCode(string host= "result", S)(S s) {
        string code= "";
        code ~= "\npublic static "~S.stringof~" decode(SerialBuff buff) {";
        code ~= _genCode(s).replace("\n", "\n    ");
        code ~= "\n};";
        return code;
    };
};

static struct SerialEncoderMethodImpl(Bitness bitness, StorePrimitivesIn spi) {
    import std.array;
    import std.meta;
    import std.traits;
    private static string _genCode(string host= "this", S)(S s) {
        string code= "";
        code ~= "\n"~S.stringof~" result= "~S.stringof~"();";
        code ~= SerialBuffWriterImpl!(bit32,HEAD).genCode(s);
        code ~= "\nreturn result;";
        return code;
    };
    private static string genCode(string host= "this", S)(S s) {
        string code= "";
        code ~= "\npublic "~S.stringof~" encode(SerialBuff buff) {";
        code ~= _genCode(s).replace("\n", "\n    ");
        code ~= "\n};";
        return code;
    };
};

static struct Serializable(Bitness bitness, StorePrimitivesIn spi) {
    import std.array;
    import std.meta;
    import std.traits;
    private static string _genCode(string host= "this", S)(S s) {
        string code= "";
        code ~= SerialDecoderMethodImpl!(bit32,HEAD).genCode(s);
        code ~= SerialEncoderMethodImpl!(bit32,HEAD).genCode(s);
        return code;
    };
    private static string genCode(string host= "this", S)(S s) {
        string code= "";
        code ~= "\n//SerializationMethods:";
        code ~= _genCode(s).replace("\n", "\n    ");
        return code;
    };
};

// ------------------------------------------------
// module core.data.SerialBuff;

public struct SerialBuff {
    public BuffImpl super_;
    alias super_ this;
    public this(T)(T size) {
        this.super_= BuffImpl(size);
        super_.locked= true;   //   The serial-buffer should be un-editable in any way, as well as un-dupable in any way, to make sure that data-corruption is difficult to cause.  
    };
    public T[] uIntArrayOf(T)() {
        uint len= super_.readUInt();
        return ArrayUtil.arrayOf!T(len);
    };
    public T[] uLongArrayOf(T)() {
        ulong len= super_.readULong();
        return ArrayUtil.arrayOf!T(len);
    };
    public void uIntArrayOf(T)(T[] data) {
        super_.writeUInt(data.length);
    };
    public void uLongArrayOf(T)(T[] data) {
        super_.writeULong(data.length);
    };
    public T readVal(T)() {
        T[1] result;
        super_.readArr(result);
        return result[0];
    };
    public void readVal(T)(ref T value) {
        value= this.readVal!T;
    };
    public void writeVal(T)(T value) {
        super_.writeArr([value]);
    };
    public void readArr(T)(ref T[] data) {
        super_.readArr!T(data);
    };
    public void writeArr(T)(T[] data) {
        super_.writeArr(data);
    };
    public string readCStr() { return super_.readCStr(); };
    public void readCStr(ref string result) { result= super_.readCStr(); };
    public void writeCStr(string data) { super_.writeCStr(data); };
    @safe ulong reader()@property { return super_.reader(); };
    @safe ulong writer()@property { return super_.writer(); };
    @safe string hexString()@property { return super_.hexString; };
};

// ------------------------------------------------
// module <Implementation-module>;

import core.memory:pureMalloc,pureFree;
import std.array;
import std.bitmanip:swapEndian;
import std.format;

/**
Rule.No("@(safe|trusted) (public|package|protected|private) (u|)(byte|short|char|int|float|long|double) read[a-zA-Z]+\(\)");
This is to discourage confusing code.  
***
Rule.From("void read[a-zA-Z]+\((u|)(byte|short|char|int|float|long|double) [a-zA-Z]+\)");
Rule.To("void read[a-zA-Z]+\(ref (u|)(byte|short|char|int|float|long|double) [a-zA-Z]+\)");
This is to encourage readable code.  
**/

public struct BuffImpl {
    @trusted package static void* _alloc(ulong size) {
        BuffImpl* buff;
        buff= new BuffImpl();
        (*buff).selfAllocated= true;
        (*buff).offsets[2]= size;
        (*buff).offsets[5]= size;
        (*buff).data= pureMalloc(size)[0 .. (cast(ulong) size)];
        void* result= cast(void*) buff;
        return result;
    };
    @trusted private enum offsetCount= 6;
    @trusted package static BuffImpl wrap(void[] data) {
        BuffImpl result= BuffImpl();
        result.data= data;
        result.offsets[2]= data.length;
        result.offsets[4]= data.length;
        result.offsets[5]= data.length;
        return result;
    };
    package bool locked;   //   Allows an implemented buffer to be locked in order to keep data from being corrupted.  
    @trusted private ulong[offsetCount] offsets;
    @trusted package static ulong[offsetCount] dupOffsetsOf(BuffImpl buff) {
        ulong[offsetCount] result;
        foreach(i; 0 .. offsetCount)result[i]= buff.offsets[i];
        return result;
    };
    private void[] data;
    private bool selfAllocated= false;
    @trusted this(uint size) {
        this.selfAllocated= true;
        this.offsets[2]= size;
        this.offsets[5]= size;
        this.data= pureMalloc(size)[0 .. (cast(ulong) size)];
    };
    @trusted this(ulong size) {
        this.selfAllocated= true;
        this.offsets[2]= size;
        this.offsets[5]= size;
        this.data= pureMalloc(size)[0 .. (cast(ulong) size)];
    };
    @trusted package typeof(this) dup()@property {
        if(this.locked)return (cast(typeof(this)*) 0)[0];   //   Locked buffers should never be dup-able.  
        typeof(this)result= typeof(this) ();
        result.data= this.data.dup();
        result.offsets= this.offsets.dup();
        return result;
    };
    package typeof(this) sdup()@property {
        if(this.locked)return (cast(typeof(this)*) 0)[0];   //   Locked buffers should never be dup-able.  
        typeof(this)[1]result;
        result[0].data= this.data;
        result[0].offsets= this.offsets.dup();
        return result[0];
    };
    @trusted package ulong size()@property { return this.data.length; };
    deprecated package ulong dataSize()@property {
        if(this.writer() < this.reader())return 0;
        return this.writer() - this.reader(); };
    @trusted package ulong _readableRemaining()@property { return this.data.length - this.offsets[1]; };
    @trusted package ulong _writeableRemaining()@property { return this.data.length - this.offsets[4]; };
    @trusted package ulong reader()@property {
        return this.offsets[1];
    };
    package void reader(ulong offset)@property {
        if(this.locked)return;
        this.offsets[1]= offset;
    };
    @trusted package ulong writer()@property {
        return this.offsets[4];
    };
    package void writer(ulong offset)@property {
        if(this.locked)return;
        this.offsets[4]= offset;
    };
    package void[] _availableBytes() {
        if(this.locked)return null;   //   Locked buffers should never be readable.  
        return this.data[this.reader() .. this.writer()];
    };
    @trusted package ulong available() {
        return this.writer() - this.reader();
    };
    private void _read(void[] data) {
        if(this.locked)return;
        ulong r0,r1,r2= data.length;
        r0= this.offsets[1];
        r1= r0 + data.length;
        if((this.offsets[1] + r2) > this.data.length) {
            r2= this.data.length - this.offsets[1];
            r1= r0 + r2;
        };
        this.offsets[1] += r2;
        void* ptr= data.ptr;
        ptr[0 .. r2]= this.data[r0 .. r1];
        // this.offsets[1]= r1;
    };
    private void _write(void[] data) {
        if(this.locked)return;
        ulong w0,w1;
        import std.stdio;
        w0= this.offsets[4];
        w1= w0 + data.length;
        this.data[w0 .. w1]= data;
        this.offsets[4]= w1;
    };
    @trusted package void readArr(T)(T[] data) {
        if(this.locked)return;
        import std.stdio;
        if(data.length==0)return;
        ulong r0,r1;
        r0= this.offsets[1];
        r1= this.offsets[1] + data.length * T.sizeof;
        this._read(cast(void[])data);
        assert(r0 != r1, "Failed to change the read-offset!  ");
    };
    @trusted package void writeArr(T)(T[] data) {
        if(this.locked)return;
        import std.stdio;
        this._write(cast(void[]) data);
    };
    @trusted package void write(ubyte[] data) { this._write(cast(void[]) data); };
    package bool[8] readBool8() {
        bool[8] result;
        byte[] d00= new byte[1];
        this._read(cast(void[]) d00);
        result[0]= (d00[0]&0x80)>0;
        result[1]= (d00[0]&0x40)>0;
        result[2]= (d00[0]&0x20)>0;
        result[3]= (d00[0]&0x10)>0;
        result[4]= (d00[0]&0x08)>0;
        result[5]= (d00[0]&0x04)>0;
        result[6]= (d00[0]&0x02)>0;
        result[7]= (d00[0]&0x01)>0;
        return result;
    };
    package void readBool8(ref bool[8] value) { value= this.readBool8(); };
    @trusted package ulong readUIVal(int start, int stop) {
        ulong[1] result;
        ubyte* ptr= cast(ubyte*) (((start +7) /8) + this.offsets[1] + (cast(ulong) this.data.ptr));
        uint j,k,bitSize;
        bitSize= stop - start;
        assert(bitSize <= 64, "You tried to retrieve to many bits, and this can't return a value that is more than 64 bits.  ");
        foreach(i; 0 .. bitSize) {
            j= i / 8;
            k= i % 8;
            result[0] += (ptr[j] & (1 << k)) << (j *8);
        };
        return result[0];
    };
    @trusted package void writeUIVal(int start, int stop, ulong value) {
        ulong[offsetCount] frozenOffsets= this.offsets.dup;
        ulong result,jumpBack= (start +7) /8;
        ulong[] d00= new ulong[1];
        uint jumpForward= start & 7;
        this.offsets[1] += jumpBack;
        this.readArr(d00);
        ulong bitSize= stop - start;
        assert(bitSize <= 64, "You are risking a near-infinite-loop!  ");
        foreach(i; 0 .. bitSize) {
            d00[0] += (value & ((1UL << jumpForward) << i)) >> jumpForward;
        };
        this.writeArr(d00);
        this.offsets= frozenOffsets;
    };
    package byte readByte() {
        byte[] result= new byte[1];
        this._read(cast(void[]) result);
        assert(result.length == 1, "Something has happened to the result!  ");
        return result[0];
    };
    package byte readUByte() {
        ubyte[] result= new ubyte[1];
        this._read(cast(void[]) result);
        assert(result.length == 1, "Something has happened to the result!  ");
        return result[0];
    };
    @trusted package ubyte[32] readUByte32() {
        ubyte[32] result;
        this.readArr(result);
        return result;
    };
    @trusted package void readUByte32(ref ubyte[32] value) {
        value= this.readUByte32();
    };
    @trusted package void readSEUByte32(ref ubyte[32] value) {
        ubyte[] d00= this.readUByte32().dup;
        foreach(i; 0 .. 32)
            d00[i]= value[31 - i];
        value= d00;
    };
    @trusted package ubyte[512] readUByte512() {
        ubyte[512] result;
        this.readArr(result);
        return result;
    };
    @trusted package void readUByte512(ref ubyte[512] value) { value= this.readUByte512(); };
    @trusted package void readSEUByte512(ref ubyte[512] value) {
        ubyte[] d00= this.readUByte512().dup;
        foreach(i; 0 .. 512)
            d00[i]= value[511 - i];
        value= d00;
    };
    @trusted package void readUByte(ref ubyte value) { value= this.readUByte(); };
    @trusted package void readSEUByte(ref ubyte value) { value= this.readUByte().swapEndian; };
    package ushort readUShort() {
        ushort[] result= new ushort[1];
        this.readArr(result);
        return result[0];
    };
    @trusted package void readUShort(ref ushort value) { value= this.readUShort(); };
    @trusted package void readSEUShort(ref ushort value) { value= this.readUShort().swapEndian; };
    package ushort readShort() {
        short[] result= new short[1];
        this.readArr(result);
        return result[0];
    };
    @trusted package void readShort(ref ushort value) { value= this.readShort(); };
    @trusted package void readSEShort(ref ushort value) { value= this.readShort().swapEndian; };
    package uint readUInt() {
        uint[] result= new uint[1];
        this.readArr(result);
        return result[0];
    };
    @trusted package void readUInt(ref uint value) { value= this.readUInt(); };
    @trusted package void readSEUInt(ref uint value) { value= this.readUInt().swapEndian; };
    package int readInt() {
        int[] result= new int[1];
        this.readArr(result);
        return result[0];
    };
    @trusted package void readInt(ref int value) { value= this.readInt(); };
    @trusted package void readSEInt(ref int value) { value= this.readInt().swapEndian; };
    package float readFloat() {
        float[] result= new float[1];
        this.readArr(result);
        return result[0];
    };
    @trusted package void readFloat(ref float value) { value= this.readFloat(); };
    package ulong readULong() {
        ulong[] result= new ulong[1];
        this.readArr(result);
        return result[0];
    };
    @trusted package void readULong(ref ulong value) { value= this.readULong(); };
    @trusted package void readSEULong(ref ulong value) { value= this.readULong().swapEndian; };
    package long readLong() {
        long[] result= new long[1];
        this.readArr(result);
        return result[0];
    };
    @trusted package void readLong(ref long value) { value= this.readLong(); };
    @trusted package void readSELong(ref long value) { value= this.readLong().swapEndian; };
    package double readDouble() {
        double[1] result;
        this.readArr(result);
        return result[0];
    };
    @trusted package void readDouble(ref double value) { value= this.readDouble(); };
    package void writeByte(byte value) {
        byte[] result= new byte[1];
        result[0]= value;
        this.writeArr(result);
    };
    @trusted package void writeUByte(T)(T value) {
        ubyte[] result= new ubyte[1];
        result[0]= cast(ubyte) value;
        this.writeArr(result);
    };
    @trusted package void writeSEUByte(T)(T value) { this.writeUByte(value.swapEndian); };
    @trusted package void writeUByte32(ubyte[32] data) {
        this.writeArr((&data)[0 .. 1]);
    };
    @trusted package void writeSEUByte32(ubyte[32] data) {
        ubyte[32] d00;
        ulong l= 31;
        foreach(i; 0 .. 32)
            d00[i]= data[l - i];
        this.writeUByte32(d00);
    };
    @trusted package void writeUShort(T)(T value) {
        ushort[] result= new ushort[1];
        result[0]= cast(ushort) value;
        this.writeArr(result);
    };
    @trusted package void writeSEUShort(T)(T value) { this.writeUShort(value); };
    @trusted package void writeUInt(T)(T value) {
        uint[] result= new uint[1];
        result[0]= cast(uint) value;
        this.writeArr(result);
    };
    @trusted package void writeSEUInt(uint value) { this.writeUInt(value.swapEndian); };
    @trusted package void writeFloat(T)(T value) {
        this.writeArr([cast(float) value]);
    };
    @trusted package void writeULong(T)(T value) {
        ulong[] result= new ulong[1];
        result[0]= cast(ulong) value;
        this.writeArr(result);
    };
    @trusted package void writeSEULong(ulong value) { this.writeULong(value.swapEndian); };
    @trusted package void readPrimitive(T)(ref T value) {
        T[] result= new T[1];
        this.readArr(result);
        value= result[0];
    };
    @trusted package void writePrimitive(T)(T value) {
        T[] result= new T[1];
        result[0]= cast(T) value;
        this.writeArr(result);
    };
    @trusted package void readSEPrimitive(T)(ref T value) {
        T[] result= new T[1];
        this.readArr(result);
        value= result[0].swapEndian;
    };
    @trusted package void writeSEPrimitive(T)(T value) {
        T[] result= new T[1];
        result[0]= cast(T) value.swapEndian;
        this.writeArr(result);
    };
    @trusted package void readSEArr(T)(ref T[] data) {
        foreach(i, v; data)
            this.readPrimitive!T(data[i]);
    };
    @trusted package void writeSEArr(T)(T[] data) {
        foreach(i, v; data)
            this.writePrimitive!T(data[i]);
    };
    @trusted package string hexString()@property {
        char[] result= new char[(this.offsets[4] - this.offsets[1]) * 3 - 1];
        if(result.length == 0)return "";
        result[0 .. 2]= "%02x".format(this.readUByte()).replace(" ", "0");
        if(result.length == 2)return cast(string) result;
        foreach(i; 1 .. (this.offsets[4] - this.offsets[1]))
            (result.ptr + i * 3 - 1)[0 .. 3]= "_%02x".format(this.readUByte()).replace(" ", "0");
        return cast(string) result;
    };
    package string readCStr() {
        ulong ro= this.offsets[1];
        ulong wo= this.offsets[4];
        while(this.readUByte() != 0 && this.offsets[1] < wo) { };
        return cast(string) ArrayUtil.arrayOf!void(this.offsets[1] - ro);
    };
    package void readCStr(ref string result) { this.readCStr(result); };
    package void writeCStr(string result) {
        void[] data= cast(void[]) result[];
        this.writeArr(data);
        this.writeUByte(0);
    };
    package byte[] readBytes() {
        byte[] data= new byte[this._readableRemaining];
        this.readArr(data);
        return data;
    };
    @system public static void lock(BuffImpl buff) { buff.locked= true; };
    @system public static void unlock(BuffImpl buff) { buff.locked= false; };
    @trusted ~this() {
        if(this.selfAllocated)
            pureFree(this.data.ptr);
    };
};

@safe unittest {
    import std.stdio;
    writeln("/----- UnitTest of `",__MODULE__,"`.  ");
    BuffImpl buff= BuffImpl(750000);
    buff.writeArr("Hello world.  This is a string!  ");
    uint v00;
    buff.readSEPrimitive(v00);
    writeln(v00);
    writeln("\\----- End of UnitTest for ",__MODULE__,".  ");
};

// ------------------------------------------------
// module core.data.ArrayUtil;

@safe public class ArrayUtil {
    public static T[] arrayOf(T)(ulong length) {
        T[] result= new T[length];
        static if(is(T == float))foreach(i; 0 .. length) {
            result[i]= 0f;
        };
        static if(is(T == float))foreach(i; 0 .. length) {
            result[i]= 0f;
        };
        return result;
    };
    public static T[] arrayOf(T)(uint length) { return ArrayUtil.arrayOf!T(cast(ulong) length); };
};
